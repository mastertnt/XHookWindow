﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Win32Interop.WinHandles;

namespace HookControl
{
    /// <summary>
    /// This control can be used to embed an external program into a C# application.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Panel" />
    public partial class HookControl : Panel
    {
        #region Bindings

        /// <summary>
        /// Sets the window position.
        /// </summary>
        /// <param name="pHWnd">The handle for the window.</param>
        /// <param name="pHWndInsertAfter">The next window.</param>
        /// <param name="pX">The x.</param>
        /// <param name="pY">The  y.</param>
        /// <param name="pCx">The cx.</param>
        /// <param name="pCy">The cy.</param>
        /// <param name="pWFlags">The window flags.</param>
        /// <returns></returns>
        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        public static extern IntPtr SetWindowPos(IntPtr pHWnd, int pHWndInsertAfter, int pX, int pY, int pCx, int pCy, int pWFlags);

        [DllImport("user32.dll", SetLastError = true)]
        static extern UInt32 GetWindowLong(IntPtr pHWnd, int pNIndex);

        [DllImport("user32.dll")]
        static extern int SetWindowLong(IntPtr pHWnd, int pNIndex, UInt32 pDwNewLong);

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr pHWnd, int pNCmdShow);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr SetParent(IntPtr pHWndChild, IntPtr pHWndNewParent);

        // Window Styles 
        const UInt32 WS_CAPTION = 0xC00000;      // WS_BORDER or WS_DLGFRAME  
        const UInt32 WS_BORDER = 0x800000;
        const UInt32 WS_DLGFRAME = 0x400000;
        const UInt32 WS_THICKFRAME = 0x40000;
        const UInt32 WS_SIZEBOX = WS_THICKFRAME;

        const short SWP_NOZORDER = 0X004;
        const short SWP_FRAMECHANGED = 0x0020;
        const short SW_SHOW = 5;
        const short GWL_STYLE = -16;

        #endregion // Bindings.

        #region Fields

        /// <summary>
        /// Theis field stores the hooked window
        /// </summary>
        private IntPtr mHookedWindow;

        /// <summary>
        /// This field stores the timer to handle properly the resize event.
        /// </summary>
        private System.Timers.Timer mTimer = new System.Timers.Timer();

        #endregion // Fields.

        #region Properties

        /// <summary>
        /// Gets or sets the hooked window.
        /// </summary>
        /// <value>
        /// The hooked window.
        /// </value>
        protected IntPtr HookedWindow
        {
            get
            {
                return this.mHookedWindow;
            }
            set
            {
                this.mHookedWindow = value;
                uint lStyle = GetWindowLong(this.HookedWindow, GWL_STYLE);
                lStyle &= ~(WS_CAPTION | WS_BORDER | WS_DLGFRAME | WS_SIZEBOX | WS_THICKFRAME);
                SetWindowLong(this.HookedWindow, GWL_STYLE, lStyle);
                SetWindowPos(this.HookedWindow, 0, 0, 0, this.Width, this.Height, SWP_FRAMECHANGED | SWP_NOZORDER);
                ShowWindow(this.HookedWindow, SW_SHOW);
            }
        }

        /// <summary>
        /// Gets or sets the process.
        /// </summary>
        /// <value>
        /// The process.
        /// </value>
        protected Process HostedProcess
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the name of the executable.
        /// </summary>
        /// <value>
        /// The name of the executable.
        /// </value>
        public string ExeName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the arguments.
        /// </summary>
        /// <value>
        /// The name of the arguments.
        /// </value>
        public string Args
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the window title.
        /// </summary>
        /// <value>
        /// The name of the window title.
        /// </value>
        public string WindowTitle
        {
            get;
            private set;
        }

        #endregion // Properties.

        /// <summary>
        /// Initializes a new instance of the <see cref="HookControl"/> class.
        /// </summary>
        public HookControl(string pExeName, string pArgs, string pWindowTitle)
        {
            this.InitializeComponent();
            this.ExeName = pExeName;
            this.Args = pArgs;
            this.WindowTitle = pWindowTitle;
            this.Resize += this.OnResize;
            
        }

        #region Methods

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ParentChanged" /> event.
        /// </summary>
        /// <param name="pEventArgs">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnParentChanged(EventArgs pEventArgs)
        {
            this.HookedWindow = IntPtr.Zero;
            IntPtr lProcessWindowHandle = IntPtr.Zero;
            this.HostedProcess = Process.Start(this.ExeName, this.Args);

            IntPtr lPanelHandle = this.Handle;

            Thread lThread = new Thread(() =>
            {
                while (lProcessWindowHandle == IntPtr.Zero)
                {
                    Thread.Sleep(10);
                    lProcessWindowHandle = TopLevelWindowUtils.FindWindow(pWindowHandle => pWindowHandle.GetWindowText().Contains(this.WindowTitle)).RawPtr;
                }
                SetParent(lProcessWindowHandle, lPanelHandle);
                this.HookedWindow = lProcessWindowHandle;
            });

            lThread.Start();
        }

        /// <summary>
        /// Handles the Resize event of the control.
        /// </summary>
        /// <param name="pSender">The source of the event.</param>
        /// <param name="pEventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnResize(object pSender, EventArgs pEventArgs)
        {
            if (this.mTimer == null)
            {
                this.mTimer = new System.Timers.Timer(50);
            }

            // Hook up the Elapsed event for the timer. 
            this.mTimer.Stop();
            this.mTimer.Elapsed += this.OnTimerElapsed;
            this.mTimer.AutoReset = false;
            this.mTimer.Start();
        }

        /// <summary>
        /// Called when [timer elapsed].
        /// </summary>
        /// <param name="pSender">The source of the event.</param>
        /// <param name="pEventArgs">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void OnTimerElapsed(object pSender, System.Timers.ElapsedEventArgs pEventArgs)
        {
            if (this.HookedWindow != IntPtr.Zero)
            {
                SetWindowPos(this.HookedWindow, 0, 0, 0, this.Width, this.Height, SWP_FRAMECHANGED | SWP_NOZORDER);
                ShowWindow(this.HookedWindow, SW_SHOW);
            }
            this.mTimer.Stop();
        }

        #endregion // Methods.
    }
}
