﻿namespace HookWindow.TestApp
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mBStart = new System.Windows.Forms.Button();
            this.mTbCommandLine = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.mTbWindowTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.mTbArgs = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mBStart
            // 
            this.mBStart.Location = new System.Drawing.Point(31, 23);
            this.mBStart.Name = "mBStart";
            this.mBStart.Size = new System.Drawing.Size(75, 23);
            this.mBStart.TabIndex = 0;
            this.mBStart.Text = "Start";
            this.mBStart.UseVisualStyleBackColor = true;
            this.mBStart.Click += new System.EventHandler(this.mBStart_Click);
            // 
            // mTbCommandLine
            // 
            this.mTbCommandLine.Location = new System.Drawing.Point(112, 56);
            this.mTbCommandLine.Name = "mTbCommandLine";
            this.mTbCommandLine.Size = new System.Drawing.Size(511, 20);
            this.mTbCommandLine.TabIndex = 2;
            this.mTbCommandLine.Text = "calc.exe";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Command line";
            // 
            // mTbWindowTitle
            // 
            this.mTbWindowTitle.Location = new System.Drawing.Point(112, 110);
            this.mTbWindowTitle.Name = "mTbWindowTitle";
            this.mTbWindowTitle.Size = new System.Drawing.Size(165, 20);
            this.mTbWindowTitle.TabIndex = 4;
            this.mTbWindowTitle.Text = "Calculator";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Window title";
            // 
            // mTbArgs
            // 
            this.mTbArgs.Location = new System.Drawing.Point(112, 84);
            this.mTbArgs.Name = "mTbArgs";
            this.mTbArgs.Size = new System.Drawing.Size(165, 20);
            this.mTbArgs.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Args";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 260);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.mTbArgs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.mTbWindowTitle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mTbCommandLine);
            this.Controls.Add(this.mBStart);
            this.Name = "MainWindow";
            this.Text = "HookExe (nby)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button mBStart;
        private System.Windows.Forms.TextBox mTbCommandLine;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox mTbWindowTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox mTbArgs;
        private System.Windows.Forms.Label label3;
    }
}

