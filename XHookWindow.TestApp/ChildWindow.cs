﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HookWindow.TestApp
{
    /// <summary>
    /// The child window
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class ChildWindow : Form
    {
        #region Fields

        /// <summary>
        /// This field stores the hook panel to instanciance.
        /// </summary>
        private HookControl.HookControl mHookPanel;

        #endregion // Fields.

        /// <summary>
        /// Initializes a new instance of the <see cref="ChildWindow"/> class.
        /// </summary>
        public ChildWindow(string pExeName, string pArgs, string pWindowTitle)
        {
            this.InitializeComponent();

            this.SuspendLayout();
            this.mHookPanel = new HookControl.HookControl(pExeName, pArgs, pWindowTitle)
            {
                BackColor = System.Drawing.SystemColors.ActiveCaption,
                Dock = System.Windows.Forms.DockStyle.Fill,
                ForeColor = System.Drawing.SystemColors.ActiveCaption,
                Location = new System.Drawing.Point(0, 0),
                Name = "HookPanel",
                Size = new System.Drawing.Size(706, 482),
                TabIndex = 0
            };
            this.Controls.Add(this.mHookPanel);
            this.ResumeLayout(false);
        }
    }
}
