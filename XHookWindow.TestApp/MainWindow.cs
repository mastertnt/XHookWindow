﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Linq;

namespace HookWindow.TestApp
{
    /// <summary>
    /// The main window
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class MainWindow : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            if (File.Exists("config.xml"))
            {
                XElement lElement = XElement.Load("config.xml");
                XElement lCmdElement = lElement.Element("CommandLine");
                if (lCmdElement != null)
                {
                    this.mTbCommandLine.Text = lCmdElement.Value;
                }
                XElement lWindowElement = lElement.Element("WindowTitle");
                if (lWindowElement != null)
                {
                    this.mTbWindowTitle.Text = lWindowElement.Value;
                }

                XElement lArgsElement = lElement.Element("CommandArgs");
                if (lArgsElement != null)
                {
                    this.mTbArgs.Text = lArgsElement.Value;
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the mBStart control.
        /// </summary>
        /// <param name="pSender">The source of the event.</param>
        /// <param name="pEventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void mBStart_Click(object pSender, EventArgs pEventArgs)
        {
            ChildWindow lForm = new ChildWindow(this.mTbCommandLine.Text, this.mTbArgs.Text, this.mTbWindowTitle.Text);
            lForm.Show();
        }

        /// <summary>
        /// Handles the FormClosing event of the MainWindow control.
        /// </summary>
        /// <param name="pSender">The source of the event.</param>
        /// <param name="pEventArgs">The <see cref="FormClosingEventArgs"/> instance containing the event data.</param>
        private void MainWindow_FormClosing(object pSender, FormClosingEventArgs pEventArgs)
        {
            XElement lRootElement = new XElement("HookWindow");
            XElement lCmdElement = new XElement("CommandLine");
            lCmdElement.Value = this.mTbCommandLine.Text;
            XElement lWindowElement = new XElement("WindowTitle");
            lWindowElement.Value = this.mTbWindowTitle.Text;
            XElement lArgsElement = new XElement("CommandArgs");
            lArgsElement.Value = this.mTbArgs.Text;
            lRootElement.Add(lCmdElement);
            lRootElement.Add(lArgsElement);
            lRootElement.Add(lWindowElement);
            lRootElement.Save("config.xml");
        }
    }
}
